package simpleExamples;

import static org.junit.Assert.*;
import static org.junit.Assume.*;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith; 

/**
 * Parameterized tests for max, using jUnit theories.
 *
 */
@RunWith(Theories.class)
public class MaxNazaPropertyTest {

	/**
	 * input-output elements for testing max.
	 * Each sample is a tuple; the first two elements
	 * are the parameters for max, and the third is the
	 * expected return value.
	 */
	@DataPoints
	public static int[][] inputsAndOutputs = {
		{0, 0, 0},
		{1, 5, 5},
		{-5, 5, 5}
	};
	
	/**
	 * Tests that max correctly computes the max between two integer elements.
	 * It applies to tuples containing the arguments as the first two parameters,
	 * and the expected result as the third
	 * @param params is the array containing the inputs and expected output.
	 */
	@Theory
	public void testMax(int[] params) {
		
		int x = params[0];
		int y = params[1];
		int expectedResult = params[2];
		
		int result = SampleStaticRoutines.max(x, y);
		
		assertEquals(expectedResult, result);
		
	}
	
}
