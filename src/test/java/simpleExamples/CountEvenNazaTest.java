package simpleExamples;

import static org.junit.Assert.*;

import org.junit.Test;

public class CountEvenNazaTest {

	/**
	 * Method countEven should return zero on empty array.
	 */
	@Test
	public void countEvenEmptyArray() {
		int[] emptyArray = {};

		int result = SampleStaticRoutines.countEven(emptyArray);

		assertEquals(0, result);		
	}

	/**
	 * Method countEven should return zero on "all odds" array.
	 */
	@Test
	public void countEvenAllOddsArray() {
		int[] allOddsArray = {1, 3, 5, -5, 7, 21};

		int result = SampleStaticRoutines.countEven(allOddsArray);

		assertEquals(0, result);	
	}

	/**
	 * Method countEven should return one on a singleton array with an even number.
	 */
	@Test
	public void countOneEvenArray() {
		int[] oneEvenArray = { 2 };

		int result = SampleStaticRoutines.countEven(oneEvenArray);

		assertEquals(1, result);	
	}

	/**
	 * Method countEven should return two on all even array of size 2.
	 */
	@Test
	public void countAllEvenArray() {
		int[] allEvenArray = { 2, 4 };

		int result = SampleStaticRoutines.countEven(allEvenArray);

		assertEquals(2, result);	
	}


	/**
	 * Method countEven should correctly count the number of even numbers,
	 * in an array of mixed odd and even numbers. 
	 */
	@Test
	public void countMixedArray() {
		int[] mixedArray = { 1, 2, 3, 4, 5, 6 };

		int result = SampleStaticRoutines.countEven(mixedArray);

		assertEquals(3, result);	
	}

	/**
	 * countEven should throw an IllegalArgumentException, when called
	 * on a null array.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void countNullArray() {
		int[] nullArray = null;

		SampleStaticRoutines.countEven(nullArray);		
	}

}
