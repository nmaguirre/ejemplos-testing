package simpleExamples;

import static org.junit.Assert.*;
import static org.junit.Assume.*;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith; 

@RunWith(Theories.class)
public class CountEvenNazaPropertyTest {

	/**
	 * Sample int arrays to evaluate countEven method.
	 */
	@DataPoints
	public static int[][] inputsForCountEven = {
		{},
		{1},
		{1, 2, 3, 4, 5, 6},
		{2, 4},
		{-2, -7, 21, 700, 524}, 
		{-100, 100, 21, 74},
		{2, 4, 6, 8, 10}, 
		null
	};
	
	
	/**
	 * countEven should return a value within zero and array length.
	 * @param array is the array to compute evenCount on. Should be non-null.
	 */
	@Theory
	public void countEvenOutputWithinZeroAndArrayLength(int[] array) {
		
		assumeNotNull(array);
		
		int result = SampleStaticRoutines.countEven(array);	

		assertTrue(result >= 0);
		assertTrue(result <= array.length);
	}

	/**
	 * countEven should return the length of the array, for "all even" arrays.
	 * @param array is the array to compute evenCount on. Should be non-null.
	 */
	@Theory
	public void countEvenAllEven(int[] array) {
		
		assumeNotNull(array);
		
		List<Integer> intList = new ArrayList<Integer>(array.length);
		for (int i : array)
		{
		    intList.add(i);
		}
		
		assumeThat(intList.stream().map(n -> n % 2).collect(Collectors.toList()), everyItem(is(equalTo(0))));
		
		int result = SampleStaticRoutines.countEven(array);
		
		assertTrue(result == array.length);
	}
	
	/**
	 * countEven should return zero, for "all odd" arrays.
	 * @param array is the array to compute evenCount on. Should be non-null.
	 */
	@Theory
	public void countEvenAllOdd(int[] array) {
		
		assumeNotNull(array);
		List<Integer> intList = new ArrayList<Integer>(array.length);
		for (int i : array)
		{
		    intList.add(i);
		}
		assumeThat(intList.stream().map(n -> n % 2).collect(Collectors.toList()), everyItem(is(not(equalTo(0)))));
				
		int result = SampleStaticRoutines.countEven(array);	

		assertTrue(result == 0);
	}
	
}
