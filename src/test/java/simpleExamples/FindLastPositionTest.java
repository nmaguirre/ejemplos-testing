package simpleExamples;

import static org.junit.Assert.*;

import org.junit.Test;

public class FindLastPositionTest {

	@Test
	public void testFindLastPositionElemNotInArray() {
		// arrange
		int[] array = {0, 0, 0};
		int value = 1;
		
		// act
		int result = SampleStaticRoutines.findLastPosition(array, value);
		
		// assert
		assertEquals(-1, result);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testFindLastPositionNullArray() {
		// arrange
		int[] array = null;
		int value = 1;
		
		// act
		SampleStaticRoutines.findLastPosition(array, value);
		
	}
	
	@Test
	public void testFindLastPositionElemInArray() {
		// arrange
		int[] array = {0, 0, 0};
		int value = 0;
		
		// act
		int result = SampleStaticRoutines.findLastPosition(array, value);
		
		// assert
		assertEquals(2, result);
	}
	
}
