package simpleExamples;

import java.nio.charset.Charset;


/**
 * This class is a brute force cracker for keyed Ceasar encoding. 
 * 
 */

public class CeasarCracker {

	// maximum length to be tried in password cracking
	private int passwordLength;
	
	// known word that belongs to the original message, prior to encryption
	private String messageWord;
	
	// message after encryption, to be used for password cracking.
	private String encryptedMessage;
	
	// key found after decryption. It's null if nothing was found, or decryption never run.
	private int[] foundKey = null;
	
	/**
	 * Default constructor. Sets both known message word and encrypted message to "". 
	 * Maximum password length to be tried is set to 1.
	 */
	public CeasarCracker() {
		messageWord = "";
		encryptedMessage = "";
		passwordLength = 1;
	}
	
	/**
	 * Constructor that receives both known message word and encrypted message. 
	 * Maximum password length to be tried is set to 1.
	 */	
	public CeasarCracker(String encryptedMessage, String word) {
		if (word == null) throw new IllegalArgumentException("null word");
		if (!isPureAscii(word)) throw new IllegalArgumentException("non-ascii word");
		if (encryptedMessage == null) throw new IllegalArgumentException("null encrypted message");
		if (!isPureAscii(encryptedMessage)) throw new IllegalArgumentException("non-ascii encrypted message");
		if (word.length()>encryptedMessage.length()) throw new IllegalArgumentException("word longer than message");
		this.encryptedMessage = encryptedMessage;
		this.messageWord = word;
		this.passwordLength = 1;
	}

	/**
	 * Returns the maximum length to be tried in password cracking. For instance, if such length is 1, then all
	 * passwords of length 1 (values from 0 to 255) will be tried as passwords for cracking.
	 * @return the maximum length to be tried for password cracking.
	 */
	public int getPasswordLength() {
		return passwordLength;
	}

	/**
	 * Sets the maximum length of the password to be tried for deciphering the encrypted message.
	 * @param length is the new maximum length for the passwords to try for cracking.
	 */
	public void setPasswordLength(int length) {
		this.passwordLength = length;
	}

	/**
	 * Returns the known word of the message previous to encryption. This word is used to test whether a given
	 * password is able to unencrypt the encrypted message. 
	 * @return the known word of the unencrypted message. 
	 */
	public String getMessageWord() {
		return messageWord;
	}
	
	/**
	 * Sets the known word from the unencrypted message. A valid decryption will be one that, applied to the
	 * encrypted message leads to an unencrypted message containing the known word.
	 * @param word is the known word from the unencrypted message.
	 */
	public void setMessageWord(String word) {
		if (word==null) throw new IllegalArgumentException("null word");
		this.messageWord = word;		
	}

	/**
	 * Returns the encrypted message, that to be cracked. 
	 * @return the encrypted message. 
	 */
	public String getEncryptedMessage() {
		return encryptedMessage;
	}

	/**
	 * Sets the encrypted message, to be "decrypted" by brute force.
	 * @param message is the encrypted message to set for the cracker.
	 */
	public void setEncryptedMessage(String message) {
		if (message==null) throw new IllegalArgumentException("null message");
		this.encryptedMessage = message;		
	}
	
	/**
	 * Encodes message with a given key.
	 * @param message is the message to be encoded.
	 * @param key is the key used for encoding, given as an array of integer values (from 0 to 255).
	 * @return the message encoded with the provided key.
	 */
	public static String encode(String message, int[] key) {
		if (key==null || key.length == 0 ||message == null) throw new IllegalArgumentException("empty/null key or null message");
		int currKeyIndex = 0;
		String output = "";
		for (int i=0; i<message.length();i++) {
			if (key[currKeyIndex]<0 || key[currKeyIndex]>255) throw new IllegalArgumentException("invalid key");
			int code = (int) message.charAt(i);
			code = (code + key[currKeyIndex]) % 256; // assumed ascii encoding
			char outChar = Character.toChars(code)[0];
			output = output + outChar;
			currKeyIndex = (currKeyIndex + 1) % key.length; 
		}
		return output;
	}

	/**
	 * Decodes message with a given key.
	 * @param message is the message to be decoded.
	 * @param key is the key used for decoding, given as an array of integer values (from 0 to 255).
	 * @return the message decoded with the provided key.
	 */
	public static String decode(String message, int[] key) {
		if (key==null || key.length == 0 ||message == null) throw new IllegalArgumentException("empty/null key or null message");
		int currKeyIndex = 0;
		String output = "";
		for (int i=0; i<message.length();i++) {
			if (key[currKeyIndex]<0 || key[currKeyIndex]>255) throw new IllegalArgumentException("invalid key");
			int code = (int) message.charAt(i);
			code = (key[currKeyIndex]<code)?(code - key[currKeyIndex]):255-(key[currKeyIndex]-(code+1)); // assumed ascii encoding
			char outChar = Character.toChars(code)[0];
			output = output + outChar;
			currKeyIndex = (currKeyIndex + 1) % key.length; 
		}
		return output;
	}
	
	/**
	 * Attempts to decode encrypted message with the given key. Brute force decryption tries to find a key
	 * of at most this.passwordLength values (each from 0 to 255) such that the decryption of the encrypted
	 * message leads to a decrypted text that contains this.messageWord.
	 * @return true iff brute force decryption succeeded.
	 */
	public boolean bruteForceDecrypt() {
		this.foundKey = null;
		if (messageWord==null || encryptedMessage==null) throw new IllegalStateException("null known word or null encrypted message");
		if (messageWord.length() > encryptedMessage.length()) throw new IllegalStateException("word is bigger than message");
		if (this.passwordLength<=0) throw new IllegalStateException("illegal password length");
		int currKeyLength = 1;
		while (currKeyLength<=this.passwordLength) {
			int[] key = new int[currKeyLength];
			while (!lastKey(key)) {
				if (decode(this.encryptedMessage, key).contains(this.messageWord)) {
					foundKey = key;
					return true;				
				}
				key = nextKey(key);
			}
			// try with all keys of length currKeyLength
			currKeyLength++;
		}
		return false;
	}

	/**
	 * Returns the key found by the last brute force decryption. Method returns
	 * null if brute force decryption was never executed, or if key was not found.
	 * @return key found by brute force decryption (null if not found / decryption not executed)
	 */
	public int[] foundKey() {
		return this.foundKey;
	}

	
	/**
	 * Calculates the next key, in lexicographic order (keys are arrays of integer values
	 * from 0 to 255).
	 * @param key is the key for which the next is going to be computed.
	 * @return the next key of the parameter, in lexicographic order.
	 */
	private static int[] nextKey(int[] key) {
		for (int i=key.length-1;i>=0;i--) {
			if (key[i]<255) {
				key[i]++;
				return key;
			}
			else {
				// key[i]==255
				key[i] = 0;
			}
		}
		return key;
	}

	/**
	 * Tests whether a given key is the "last" key, i.e., all its values are 255.
	 * @param key is the key to be tested.
	 * @return true iff the given key is composed only of the value 255.
	 */
	private static boolean lastKey(int[] key) {
		if (key==null) throw new IllegalArgumentException();
		for (int i=0; i<key.length;i++) {
			if (key[i]!=255) return false;
		}
		return true;
	}

	/**
	 * Tests whether a string is pure ascii
	 * @param s is the string being analyzed
	 * @return true iff s is pure ascii
	 */
	public static boolean isPureAscii(String s) {
		if (s == null) throw new IllegalArgumentException("can't check isPureAscii on null");
	    return Charset.forName("US-ASCII").newEncoder().canEncode(s);
	  }



}
